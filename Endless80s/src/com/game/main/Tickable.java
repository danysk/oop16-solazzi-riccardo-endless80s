package com.game.main;

public interface Tickable {
	void tick();
}
